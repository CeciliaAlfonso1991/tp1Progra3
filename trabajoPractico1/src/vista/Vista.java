package vista;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import intermediario.Adivino;
import intermediario.Juego;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;

public class Vista {

	private JFrame frame;
	private Adivino adivino;
	private Juego juego;

	// botones
	private JButton btnSi;
	private JButton buttonCerrar;
	private JButton button_1;
	private JLabel lblNewLabel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vista window = new Vista();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Vista() {

		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		adivino = new Adivino();
		juego = new Juego();
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Adivino");

		btnSi = new JButton("si");
		btnSi.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				String respuesta = btnSi.getText();
				juego.escucharRespuesta(respuesta);
			}
		});

		btnSi.setBounds(41, 352, 89, 47);
		frame.getContentPane().add(btnSi);

		JButton btnNo = new JButton("no");
		btnNo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

			}

		});
		btnNo.setBounds(140, 352, 89, 47);
		frame.getContentPane().add(btnNo);

		JButton btnNoS = new JButton("no s\u00E9");
		btnNoS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

			}
		});
		btnNoS.setBounds(239, 352, 89, 47);
		frame.getContentPane().add(btnNoS);

		buttonCerrar = new JButton("Cerrar");
		buttonCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
			}
		});
		buttonCerrar.setBounds(155, 503, 125, 47);
		frame.getContentPane().add(buttonCerrar);

		button_1 = new JButton("Ingresar pais");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				adivino.ingresarPaisPensado();

			}
		});
		button_1.setBounds(20, 503, 125, 47);
		frame.getContentPane().add(button_1);

		JLabel lblNewLabel = new JLabel(juego.pregunta());
		lblNewLabel.setBackground(Color.ORANGE);
		lblNewLabel.setFont(new Font("Square721 BT", Font.PLAIN, 15));
		lblNewLabel.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewLabel.setIcon(new ImageIcon(Vista.class.getResource("/vista/talk.png")));
		lblNewLabel.setBounds(10, 0, 494, 410);
		frame.getContentPane().add(lblNewLabel);

		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(Vista.class.getResource("/vista/genie.png")));
		lblNewLabel_1.setBounds(420, 11, 364, 550);
		frame.getContentPane().add(lblNewLabel_1);

	}

	public Window vistaFrame() {
		// TODO Auto-generated method stub
		return frame;
	}
}

