package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import intermediario.Juego;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Menu {

	private JFrame frame;  
	private Vista vista;
	private Juego met;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Menu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		vista = new Vista();
		met = new Juego();

		JButton btnJugar = new JButton("JUGAR");
		btnJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				vista.vistaFrame().setVisible(true);
			}
		});

		btnJugar.setBounds(307, 287, 170, 23);
		frame.getContentPane().add(btnJugar);

		JButton acercaDe = new JButton("REGLAS");
		acercaDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				met.mostrarReglas();
			}
		});
		acercaDe.setBounds(307, 321, 170, 23);
		frame.getContentPane().add(acercaDe);

		JButton salir = new JButton("SALIR");
		salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
			}
		});
		salir.setBounds(307, 355, 170, 22);
		frame.getContentPane().add(salir);

	}

}
