package intermediario;

import java.util.HashMap;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import informacion.DataManager;
import informacion.DataPais;

public class Adivino {
	//este actualiza toda la informacion que ingresa de la interfaz
	private DataManager dManager;
	private LinkedList<DataPais> paises;
	private LinkedList<String> preguntas;
	private HashMap<String,Integer> modas;

	private String ingresoPais, modaMedia;
	private boolean tieneCualidad, respuesta;
	private int suma, cantidad, referencia;
	
	public Adivino() {
		dManager = new DataManager();
		paises=dManager.traerTodosPaises();
		preguntas= new LinkedList<String>() ;
		tieneCualidad=false;
	}

	
	public void generarPregunta() {
		reiniciarModas();
		analizarModas();
		calcularModaMedia();		
	}
	public String paisAdivinado() {
		if(!ingresoPais.equals("")) {
			return verificarPais();
		}
		return paisGanador();
	}
	
	
	private String paisGanador() {
		// TODO Auto-generated method stub
		if(paises.size()>10) {
			return "";//preguntar();
		}
		return paises.getFirst().obtenerNombre();
	}

	public String preguntar() {
		/**if(preguntas.contains(modaMedia)) {
			generarPregunta();//para que no se repitan las preguntas
		}**/
		generarPregunta();
		preguntas.add(modaMedia);
		return "¿ "+modaMedia+" ?";
	}
	
	public void escucharRespuesta(boolean respuesta) {
		this.respuesta = respuesta;
	}
	
	
	public void descartarPaises() {
		paises.forEach((dataPais)-> {
			tieneCualidad = false;
			dataPais.obtenerCualidades().forEach((cualidad)->
				tieneCualidad |= modaMedia.equals(cualidad));
			if(respuesta ^ tieneCualidad)
				paises.remove(dataPais);				
		});		
	}
	
	private void reiniciarModas() {
		modas = new HashMap<String,Integer>();
	}
	
	private void analizarModas() {
		paises.forEach((dataPais) ->{
			dataPais.obtenerCualidades().forEach(cualidad ->{
				if(!modas.containsKey(cualidad))
					modas.put(cualidad, 0);
				modas.put(cualidad, modas.get(cualidad)+1);				
			});
		});
	}
	
	private void calcularModaMedia() {
		suma = 0; cantidad = 1;
		modas.forEach((moda, nivel)-> {
			cantidad++;
			suma+=nivel;
		});		
		int prom = suma/cantidad;
		
		referencia = 1000;
		modas.forEach((moda, nivel)-> {
			if(Math.abs(nivel - prom) < referencia) {
				referencia = Math.abs(nivel - prom);
				modaMedia = moda;
			}
		});
	}
	
	private String verificarPais() {
		DataPais pais= new DataPais(ingresoPais,"",null,0);
		verificarSiEstaPaisIngresado(pais);
		return ingresoPais;
	}

	private void verificarSiEstaPaisIngresado(DataPais pais) {
		if(!estaPais(pais)) {
			throw new IllegalArgumentException("Pais ingresado no existe");
		}
	}
	
	private boolean estaPais(DataPais ingreso) {
		for(DataPais pais: paises) {
			if(pais.obtenerNombre().equals(ingreso.obtenerNombre())) {
				return true;
			}
		}
		return false;
	}
	
	public void ingresarPaisPensado() {
		ingresoPais = JOptionPane.showInputDialog("Ingrese el pais que penso");
	}
}
