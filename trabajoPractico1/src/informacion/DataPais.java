package informacion;

import java.util.LinkedList;


public class DataPais {
	private String nombre;
	private LinkedList<String> atributos;
	private String continente;
	private int popularidad;
	
	static String atributosEnLinea;
	
	public DataPais(String nombre, String continente, LinkedList<String> atributos, int popularidad){
		this.nombre=nombre;
		this.atributos=atributos;
		this.continente=continente;
		this.popularidad = popularidad;
	}

	public LinkedList<String> obtenerCualidades(){
		return atributos;
	}
	
	public String obtenerNombre() {
		return nombre;
	}
	
	public String obtenerContinente() {
		return continente;
	}
	
	public void agregarCualidades(LinkedList<String> cualidades) {
		this.atributos=cualidades;
	}
	
	public void modificarContinente(String nombre) {
		this.continente=nombre;
	}
	
	public void modificarNombre(String nombre) {
		this.nombre=nombre;
	}
	@Override
	public String toString() {
		atributosEnLinea = "";
		atributos.forEach(a -> {
			atributosEnLinea += a + " ";
		});
		return nombre + continente + atributosEnLinea + popularidad;
	}
}
