package informacion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;

public class DataManager {

	private LinkedList<DataPais> paises;
	private LinkedList<String> preguntas;
	private File fileContientes, filePaises;

	private FileReader fileReader;
	private BufferedReader buffReader;

	private FileWriter fileWriter;
	private PrintWriter printWriter;

	public DataManager() {
		paises = new LinkedList<DataPais>();
		preguntas = new LinkedList<String>();
		fileContientes = new File("continentes.txt");
		filePaises = new File("paises.txt");
	}

	public LinkedList<DataPais> traerTodosPaises() {
		LinkedList<DataPais> paises = new LinkedList<DataPais>();
		try {
			filePaises = new File("listadeatributos.txt");
			fileReader = new FileReader(filePaises);
			buffReader = new BufferedReader(fileReader);

			while ((buffReader.readLine()) != null) {
				String nombre = buffReader.readLine();
				String continente = buffReader.readLine();
				String cualidades = buffReader.readLine();
				String cualidad = "";
				LinkedList<String> cualidadez = new LinkedList<String>();
				for (int i = 0; i + 1 < cualidades.length(); i++) {
					if (cualidades.charAt(i) == '+') { // debe terminar la linea con + para que cuente la ultima cualidad
						cualidadez.add(cualidad);
						cualidad = "";
						i++;
					}
					cualidad += cualidades.charAt(i);
				}
				int popularidad = Integer.parseInt(buffReader.readLine());
				paises.add(new DataPais(nombre,continente, cualidadez, popularidad));
			
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fileReader != null)
					fileReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return paises;
	}

	public void guardarPaises() {

	}

	public LinkedList<String> obtenerPreguntas() {
		return preguntas;
	}

}
